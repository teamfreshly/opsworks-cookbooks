node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  bash "set_permissions" do
    user 'root'
    cwd "#{app_root}/wp-content"
    code <<-EOH
      mkdir uploads
      chmod -R 0777 uploads/
      chown -R deploy:www-data uploads/
      EOH
    only_if { File.directory?("#{app_root}/wp-content") }
  end
end
