
Regarding beta_wordpress_permissions, the recipe to execute is: beta_wp_perms::set

Below is for the cron_jobs recipe:
```json
{
    "custom_env": {
        "cron_jobs": [  
            {
                "name"      : "Send an email every sunday at 8:10",
                "minute"    : "10",
                "hour"      : "8",
                "day"       : "*",
                "month"     : "*",
                "weekday"   : "6",
                "command"   : "cd /srv/www/staging_site/current && php .lib/mailing.php"
            },
            {
                "name"      : "Run at 8:00 PM every weekday Monday through Friday ONLY in November",
                "minute"    : "0",
                "hour"      : "20",
                "day"       : "*",
                "month"     : "10",
                "weekday"   : "1-5",
                "command"   : "cd /srv/www/staging_site/current && php app/console command:start:jobs"
            },
            {
                "name"      : "Run Every 12 Hours - 1AM and 1PM",
                "minute"    : "*",
                "hour"      : "1-13",
                "day"       : "*",
                "month"     : "*",
                "weekday"   : "*",
                "command"   : "cd /srv/www/production_site/current && php app/console hello:world"
            },
            {
                "name"      : "Run every 15 minutes",
                "minute"    : "*/15",
                "hour"      : "*",
                "day"       : "*",
                "month"     : "*",
                "weekday"   : "*",
                "command"   : "cd /srv/www/production_site/current && php app/console memory:leak"
            },
        ]
    }
}
```