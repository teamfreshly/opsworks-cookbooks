name        "freshly_redesign_deps"
description 'Configures Freshly environment for redesign dependencies'
maintainer  "Betastream"
license     "Apache 2.0"
version     "1.0.0"

depends 'nodejs'
