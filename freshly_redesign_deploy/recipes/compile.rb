node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  bash "compile_project" do
    environment ({ 'HOME' => ::Dir.home('deploy'), 'USER' => 'deploy' })
    user 'deploy'
    group 'www-data'
    code <<-EOH
      cd #{app_root}/frontend
      npm install --no-bin-links
      bower install --allow-root --config.interactive=false
      gulp dist
      EOH
    only_if { File.directory?("#{app_root}/frontend") }
  end
end
